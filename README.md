## Usage

 1. Build the executables:
```bash
$ cargo build --release
```
 2. Generate the index file using the index command:
```bash
$ ./target/release/index /path/to/pwned-passwords-sha1-ordered-by-hash-v4.txt /path/to/output/index
```
 3. Run the search command and input your password:
```bash
$ ./target/release/search /path/to/index /path/to/pwned-passwords-sha1-ordered-by-hash-v4.txt 
```

use sha1::{Digest, Sha1};
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::{stdin, BufReader, SeekFrom};

fn as_u64(buf: &[u8]) -> u64 {
    buf[0] as u64
        | ((buf[1] as u64) << 8)
        | ((buf[2] as u64) << 16)
        | ((buf[3] as u64) << 24)
        | ((buf[4] as u64) << 32)
        | ((buf[5] as u64) << 40)
        | ((buf[6] as u64) << 48)
        | ((buf[7] as u64) << 56)
}

fn fetch_index(index: &mut std::fs::File, entry_size: u64, i: u64) -> std::io::Result<u64> {
    index.seek(SeekFrom::Start(entry_size * i))?;
    let mut buf = vec![0u8; 8];
    index.read_exact(&mut buf)?;
    Ok(as_u64(&buf))
}

fn print_usage() {
    println!("USAGE: search [INDEX FILE] [ARCHIVE]");
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() > 2 {
        let entry_size = std::mem::size_of::<u64>() as u64;
        let mut index = File::open(&args[1])?;
        let index_size = index.metadata()?.len();
        let nentries = index_size / entry_size;

        let mut archive = BufReader::new(File::open(&args[2])?);

        let mut pwd = String::new();
        stdin().read_line(&mut pwd)?;
        pwd = pwd.trim().to_string();

        let mut hasher = Sha1::new();
        hasher.input(pwd);
        let hash = format!("{:x}", hasher.result()).to_uppercase();

        let mut lower = 0;
        let mut upper = nentries;
        let mut found = false;
        while lower < upper {
            let mid = (upper + lower) / 2;
            let imid = fetch_index(&mut index, entry_size, mid)?;
            archive.seek(SeekFrom::Start(imid))?;

            let mut line = String::new();
            archive.read_line(&mut line)?;

            // remove colon, occurrences count and newline
            let pos = line.rfind(':').unwrap();
            line.truncate(pos);

            if line == hash {
                found = true;
                break;
            } else if line > hash {
                upper = mid;
            } else if line < hash {
                lower = mid + 1;
            }
        }

        if found {
            println!("Hash is \x1b[31mPRESENT!\x1b[0m :(");
        } else {
            println!("Hash is *NOT* present! :)");
        }
    } else {
        print_usage();
    }
    Ok(())
}

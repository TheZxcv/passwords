use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::stdout;
use std::time::Instant;

fn as_u8_slice(v: &[u64]) -> &[u8] {
    unsafe {
        std::slice::from_raw_parts(
            v.as_ptr() as *const u8,
            v.len() * std::mem::size_of::<u64>(),
        )
    }
}

fn print_usage() {
    println!("USAGE: index [ARCHIVE] [OUTPUT INDEX]");
}

const MIN_ENTRY_SIZE: usize = 42 /* hash + colon + one digit */;

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() > 2 {
        let mut archive = File::open(&args[1])?;
        let archive_size = archive.metadata()?.len() as usize;
        println!("Archive size: {}", archive_size);

        let output_buffer_len = archive_size / MIN_ENTRY_SIZE;
        println!("Estimated output size: {}", output_buffer_len);
        let mut output_buf = vec![0u64; output_buffer_len];

        let mut buf = vec![0u8; 4194304 /* 4MiB */];
        let mut curr_line = 1;
        let mut off: u64 = 0;
        let mut last_off: u64 = 0;

        output_buf[curr_line - 1] = off;
        let mut now = Instant::now();
        loop {
            let bytes_read: usize = archive.read(&mut buf)?;

            if bytes_read == 0 {
                break;
            }

            for &byte in buf.iter().take(bytes_read) {
                off += 1;
                if byte == 0x0a {
                    curr_line += 1;
                    output_buf[curr_line - 1] = off;
                }
            }

            let elapsed_ms =
                now.elapsed().as_secs() * 1000u64 + now.elapsed().subsec_nanos() as u64 / 1_000_000;
            if elapsed_ms > 1000 {
                let bytes_per_ms = (off - last_off) / elapsed_ms;
                let speed = bytes_per_ms as f64 * (1000f64) / (1024f64 * 1024f64);
                print!(
                    "\rprogress {:.02}% -- speed {:.02} MiB/sec",
                    100f64 * (off as f64 / archive_size as f64),
                    speed
                );
                stdout().flush()?;

                now = Instant::now();
                last_off = off;
            }
        }

        // discard unused entries
        output_buf.truncate(curr_line);
        File::create(&args[2])?.write_all(as_u8_slice(&output_buf))?;
    } else {
        print_usage();
    }
    Ok(())
}
